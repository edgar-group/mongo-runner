module.exports = {
    MONGO_CONNECTION_STRING: 'mongodb://localhost:27017',
    PUBLIC: 0,
    TIMEOUT: 2500,
    URL: '/mongo/hr',
    DB_NAME: 'dbname-here'
};