query=db.cards.mapReduce (
        `function() {
                if (this.subtypes !== undefined)
                    this.subtypes.forEach( function(subtype) {            
                        emit( subtype, {count : 1} );                                       
                    }
            );  
            };`,
        
);
