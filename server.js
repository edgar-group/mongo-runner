'use strict';
// server.js
const http = require('http');
const express = require('express'); // call express
const app = express(); // define our app using express
const bodyParser = require('body-parser');
const winston = require('winston');
winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {
    'timestamp': true
});
if (process.env.NODE_ENV !== 'production') {
    winston.level = 'debug';
} else {
    winston.level = 'info';
}

const config = require('./config/config');
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const safeEval = require('safe-eval')
    // configure app to use bodyParser()
    // this will let us get the data from a POST
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());


var port = process.env.PORT || 20080; // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router(); // get an instance of the express Router
var instance = ((parseInt(process.env.pm_id) || 0) % 5) + 1;
//instance = 6; // dok ne doznam kako se baze razdese :(
// if (config.PG_CONNECTION_STRING.endsWith("0") && config.db.endsWith("0")) {
//     config.PG_CONNECTION_STRING += instance;
//     config.db += instance;
// }

config.TIMEOUT = config.TIMEOUT || 2500;


function handleFind(query, res, cb) {
    let collectionName;
    let nl1 = query.indexOf('.find');
    collectionName = parseCollectionName(query, res, 'find');
    if (collectionName) {
        let nl1, nl2;
        nl1 = query.indexOf('(', nl1);
        nl2 = query.lastIndexOf(')');
        let args = query.substring(nl1 + 1, nl2);
        if (args.toLowerCase().indexOf('function') &&
            args.replace(/\s+/g, '').indexOf(')()') >= 0) {
            res.json({
                success: false,
                data: {},
                db: config.db,
                error: {
                    message: 'Potential immediate function invocation in .find() query. Logged.'
                }
            });
            if (cb) cb();
            return;
        }
        try {
            args = safeEval(`[${args}]`); // eval is evil, I know...
        } catch (parseError) {
            res.json({
                success: false,
                data: [],
                db: config.DB_NAME,
                error: {
                    message: `Could not parse find arguments: [${args}]
                              Error message:${parseError.message}`
                }
            });
            if (cb) cb();
            return;
        }

        if (args.length === 0) args = [{}, {}];
        if (args.length === 1) args.push({});
        MongoClient.connect(config.MONGO_CONNECTION_STRING, function(err, client) {
            //assert.equal(null, err);
            const db = client.db(config.DB_NAME);
            const coll = db.collection(collectionName);
            let findGUID = `find${guid()}`;
            console.time(findGUID);
            coll.find(args[0], args[1]).toArray(function(err, docs) {
                //assert.equal(err, null);
                winston.debug('find: docs.length', docs && docs.length);
                console.timeEnd(findGUID);
                if (docs && docs.length && docs.length > 1000) {
                    docs.splice(1000);
                }
                res.json({
                    success: err === null,
                    data: docs,
                    db: config.DB_NAME,
                    error: err ? {
                        message: err.errmsg
                    } : undefined
                });
                client.close();
                if (cb) cb();
            });
        });
    }
}

function parseCollectionName(query, res, functionName) {
    let nl1;
    let collectionName;
    functionName = functionName.toLowerCase();
    nl1 = query.toLowerCase().indexOf(`.${functionName}`);
    let header = query.substring(0, nl1 + `.${functionName}`.length);
    header = header.split('.');
    let i = 0;
    while (!header[i].toLowerCase().startsWith(functionName)) {
        if (i > 10) {
            res.json({
                success: false,
                data: {},
                db: config.db,
                error: {
                    message: `Could not parse .${functionName}() query: \n${query}`
                }
            });
            return;
        }
        ++i;
    }
    if (i > 0) {
        collectionName = header[i - 1];
    } else {
        res.json({
            success: false,
            data: {},
            db: config.db,
            error: {
                message: `Could not parse (find collection name) in .${functionName}() query.`
            }
        });
        return;
    }
    if (query.lastIndexOf(')') === -1) {
        res.json({
            success: false,
            data: {},
            db: config.db,
            error: {
                message: `Could not parse (find the closing parenthesis) in .${functionName}() query.`
            }
        });
        return;
    }
    return collectionName;
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4();
}
async function handleMR(query, res) {
    let collectionName;
    let nl1 = query.indexOf('db.assertion.');
    let assertionQuery;
    if (nl1 > 0) {
        assertionQuery = query.substring(nl1);
        query = query.substring(0, nl1);
    }
    collectionName = parseCollectionName(query, res, 'mapReduce');
    if (collectionName) {
        let nl1, nl2;
        nl1 = query.indexOf('(', nl1);
        nl2 = query.lastIndexOf(')');
        let args = query.substring(nl1 + 1, nl2);
        if (args.toLowerCase().indexOf('function') &&
            args.replace(/\s+/g, '').indexOf(')()') >= 0) {
            res.json({
                success: false,
                data: {},
                db: config.db,
                error: {
                    message: 'Potential immediate function invocation in .mapReduce() query. Logged.'
                }
            });
            return;
        }
        try {
            args = safeEval(`[${args}]`); // eval is evil, I know...
        } catch (parseError) {
            res.json({
                success: false,
                data: [],
                db: config.DB_NAME,
                error: {
                    message: `Could not parse M/R arguments: ${parseError.message}`
                }
            });
            return;
        }
        console.log(args);
        if (args.length != 2 && args.length != 3) {
            res.json({
                success: false,
                data: {},
                db: config.db,
                error: {
                    message: '.mapReduce() should have at least 2 arguments (eg. map and reduce function), and at most 3 arguments.'
                }
            });
            return;
        }
        let subArgs = {
            out: `mrTmpColl${guid()}`
        };
        if (args.length === 3) {
            subArgs.finalize = args[2].finalize,
                subArgs.query = args[2].query;
            subArgs.sort = args[2].sort;
            subArgs.limit = args[2].limit;
        }
        winston.debug('***********************************');
        winston.debug('mr.args', args);
        winston.debug('mr.subArgs', subArgs);
        winston.debug('***********************************');
        console.time(subArgs.out);
        MongoClient.connect(config.MONGO_CONNECTION_STRING, function(err, client) {
            assert.equal(null, err);
            const db = client.db(config.DB_NAME);
            const coll = db.collection(collectionName);
            coll.mapReduce(
                args[0],
                args[1],
                subArgs,
                function(err, results) {
                    if (err) {
                        winston.error(err);
                        res.json({
                            success: false,
                            data: {},
                            db: config.db,
                            error: {
                                message: err.errmsg
                            }
                        });
                        client.close();
                        return;
                    }
                    winston.debug('Fetching data from tmp collection: ', subArgs.out);
                    const mrTempColl = db.collection(subArgs.out);
                    console.timeEnd(subArgs.out);
                    if (assertionQuery) {
                        assertionQuery = assertionQuery.replace('db.assertion', `db.${subArgs.out}`);
                        handleFind(assertionQuery, res, function() {
                            mrTempColl.drop(function(err, docs) {
                                if (err) {
                                    winston.error('Error droping collection ', subArgs.out, err);
                                } else {
                                    winston.debug('Dropped collection ', subArgs.out);
                                }
                                client.close();
                            });
                        });
                    } else {
                        mrTempColl.find().toArray(function(err, docs) {
                            winston.debug('docs.count = ', docs && docs.length);
                            res.json({
                                success: err === null,
                                data: docs,
                                db: config.DB_NAME,
                                error: err ? {
                                    message: err.errmsg
                                } : undefined
                            });
                            mrTempColl.drop(function(err, docs) {
                                //assert.equal(err, null);
                                if (err) {
                                    winston.error('Error droping collection ', subArgs.out, err);
                                } else {
                                    winston.debug('Dropped collection ', subArgs.out);
                                }
                                client.close();
                            });

                        });
                    }

                }
            );
        });
    }
}

winston.debug(`Mounting: ${config.URL}`);
router.post(`${config.URL}`, function(req, res) {
    // TODO remove stop words!
    //  This is unsecure by design
    winston.debug('req.body\n', req.body);
    var query = req.body.query;
    if (!query) {
        res.json({
            success: false,
            data: {},
            db: config.db,
            error: {
                message: 'Query not defined, nothing to execute.'
            }
        });
    } else {
        // Use connect method to connect to the server
        // Must be in this order bcs MR can have trailing assertion query
        if (query.toLowerCase().indexOf('.mapreduce') !== -1) {
            handleMR(query, res);
        } else if (query.indexOf('.find') !== -1) {
            handleFind(query, res);
        } else {
            res.json({
                success: false,
                data: {},
                db: config.db,
                error: {
                    message: 'Could not parse query: unknown command.'
                }
            });
        }


    }

});

app.use(router);

var server = http.createServer(app);

if (config.PUBLIC) {
    server.listen(port); // , '0.0.0.0'
} else {
    server.listen(port, 'localhost');
}

server.on('error', onError);
server.on('listening', onListening);

function onError(error) {
    winston.error('Global onerror handler:', error);
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    // var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    winston.info(' Running the mongo runner on: ' + JSON.stringify(addr));
    winston.info(' Config is: ' + JSON.stringify(config));
    winston.info(' Instance is ' + instance);
    winston.info(' Log level is ' + winston.level);
}