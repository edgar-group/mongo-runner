
# mongo-runner

Mongo-runner is a runner for MongoDB for Edgar.  
This project is needed if you want to test Mongo's `find` and `mapReduce` statements.


## Setup 
 * `git clone git@gitlab.com:edgar-group/mongo-runner.git`
 * `npm install`
 * Create files (by copying and adjusting `config/development-config-TEMPLATE.js`):
   * `config/development-config.js`
   * `config/production-config.js`
 * Run mongo-runner with: `npm start`

